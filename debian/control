Source: kvazaar
Section: libs
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Joachim Bauch <fancycode@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 ffmpeg,
 hm,
 libcrypto++-dev,
 pkgconf
Build-Depends-Indep:
 doxygen,
 graphviz
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/ultravideo/kvazaar
Vcs-Browser: https://salsa.debian.org/multimedia-team/kvazaar
Vcs-Git: https://salsa.debian.org/multimedia-team/kvazaar.git

Package: kvazaar
Section: video
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: HEVC encoder - application
 kvazaar is a HEVC encoder.
 .
 This package contains the kvazaar application.

Package: libkvazaar7
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: HEVC encoder - shared library
 kvazaar is a HEVC encoder.
 .
 This package contains the shared library.

Package: libkvazaar-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libkvazaar7 (= ${binary:Version}),
 ${misc:Depends}
Suggests:
 kvazaar-doc
Description: HEVC encoder - development files
 kvazaar is a HEVC encoder.
 .
 The development headers for compiling programs that use kvazaar are provided
 by this package.

Package: kvazaar-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends}
Description: HEVC encoder - documentation
 kvazaar is a HEVC encoder.
 .
 This package contains the documentation.
